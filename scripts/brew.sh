#!/usr/bin/env bash

# Install command-line tools using Homebrew.

# Ask for the administrator password upfront.
sudo -v

# Keep-alive: update existing `sudo` time stamp until the script has finished.
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

# Install brew
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# Check if all is well with brew
brew doctor

# Make sure we’re using the latest Homebrew.
brew update

# Upgrade any already-installed formulae.
brew upgrade

# Install Bash 4.
# Note: don’t forget to add `/usr/local/bin/bash` to `/etc/shells` before
# running `chsh`.
brew install bash
brew install bash-completion

# Install more recent versions of some OS X tools.
brew install vim --override-system-vi
brew install homebrew/dupes/grep

# Install other useful binaries.
brew install the_silver_searcher 
brew install git

# Install Tig for Git
brew install tig

# Install Mutt to check mails
brew install mutt

# Install tmux
echo "Installing tmux..."
brew install tmux
echo "Symlinking .tmux.conf file to home directory..."
ln -s ~/dotfiles/tmux/.tmux.conf ~/.tmux.conf

# Install Tmuxinator
echo "Installing tmuxinator..."
gem install tmuxinator
tmuxinator doctor
ln -s ~/dotfiles/tmuxinator/.tmuxinator ~/.tmuxinator

# Install brew cask to easily install software with the command line
brew install caskroom/cask/brew-cask
# To make sure Alfred can open these apps 
# (because they are not really in /Applications but are symlinked from /opt/homebrew-cask/Caskroom/)
# we run the following link command
brew cask alfred link
# For installing beta versions like Chrome Canary etc we must tap the versions
brew tap caskroom/versions

# Install Ctags (for use in Vim with CtrlP)
brew install ctags

# Install Node.js. Note: this installs `npm` too, using the recommended
# installation method.
brew install node

# Remove outdated versions from the cellar.
brew cleanup

# Install Grunt (Javascript Task Runner) globally
echo "Installing grunt globally..."
npm install -g grunt-cli
