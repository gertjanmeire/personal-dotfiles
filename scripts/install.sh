# Apps
apps=(
  filezilla
  slate
  spotify
  sublime-text3
  virtualbox
  vlc
  quicklook-json
  skype
)

# Install apps to /Applications
# Default is: /Users/$user/Applications
echo "installing apps..."
brew cask install --appdir="/Applications" ${apps[@]}
