# Horizontal composition of behavior

- Huidige klasse heeft voorrang op een Trait die gebruikt wordt!
- Trait heeft voorrang op een parent method in een inheritance structuur!
- 2 traits met dezelfde method = fatal error
    --> oplossing met "insteadof" keyword.
    --> omzeiling met "as" keyword.
