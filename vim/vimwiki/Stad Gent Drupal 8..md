# Stad Gent Drupal 8.

Confluence page:
https://digipolisgent.atlassian.net/wiki/spaces/SG8/pages

Design:
https://app.zeplin.io/project/5a7973af3882dd06217f2015
===============================================================================

## Notities 05-03-2018
Eerste design meeting (design sprint 1).

### Kleuren
Worden al doorgevoerd voor links en buttons zodat alles WCAG2AA compatibel is.

BUTTONS: Voor buttons moeten we een icoon voorzien voor error / correct buttons.
LINKS:  Er komt nog een icoon voor externe links bij.

### Forms
Er moet nog een icoon bij foute ingevulde velden.

### Fonts
Boldness zal overal 1 type bold zijn --> semibold
Light gebruiken we liever niet.
Cursief zullen 1 of 2 types zijn.
--> Dit valt nog te finaliseren door Jessie.

### Quotes
Quotes kunnen heel lang worden --> kleur zal moeten aangepast worden.

### Breakpoints
Wordt nog besproken welke breakpoints pecies gedefinieerd zullen worden.

### Buttons
Subtiele animatie ivm schaduwen op knoppen moeten voorzien worden.
--> dit is beter voor usability.

BUTTON KLEUREN:
De groene buttons moet nog een WCAG compliant kleur krijgen.
Het grijze kleur voor buttons zou Jessie donkerder maken.

### Paragrafen / interlinies e.d.
Moeten nog besproken worden.
-------------------------------------------------------------------------------


## Notities 05-03-2018
Meeting met Jessie ivm uitwerking van eerste design componenten.

Zaken die voorlopig al opgenomen kunnen worden in 8.x-3.x-dev
- Kleuren zijn finaal --> 4 basiskleuren en 8 varianten elk met gradaties
  a.d.h.v. opacity percentages.
- Logo in verschillende kleuren --> a.d.h.v. de kleuren.
- Favicon in verschillende kleuren--> a.d.h.v. de kleuren.
- Fonts (iconfont)
-------------------------------------------------------------------------------


## Notities 26-02-2018
Eerste sprint 5 mandagen ingepland. Zal voorlopig grotendeels stijlgids
implementatie zijn van de reeds gedesignede elementen.

FOCUS EERSTE SPRINT:
  Design implementatie in de stijlgids
-------------------------------------------------------------------------------
