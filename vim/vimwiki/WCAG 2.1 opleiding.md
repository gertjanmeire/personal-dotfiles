# WCAG 2.1 opleiding

Mindmap: opleiding gevolgd via skype ivm update naar WCAG 2.1

Vergelijking met WCAG 2.0:
https://www.w3.org/TR/WCAG21/#comparison-with-wcag-2-0

Drupal?
https://www.drupal.org/project/ideas/issues/2864791

1.3.5 invoerelementen --> attributen op input elementen toepassen.
https://www.w3.org/TR/WCAG21/#input-purposes

2.5.2 Label in name --> bv. op onze lees meer links doen we dit fout.
--> op te nemen in stijlgids.

4.1.3 Aria live regions gebruiken om zaken die bv. via ajax gebeuren te melden
zonder de focus te veranderen voor bv. blinde gebruikers.

1.4.10 Inzoomen tot 400% en alles moet blijven werken --> equivalent van 320px
reponsive design.

1.4.11 Non text contrast --> alle belangrijke visuele velden met functie moeten
contrast 3:1 hebben.

1.4.13 Content on Hover or Focus --> moet zichtbaar zijn bij bv. hoveren.
Bv. popup met tekst bij hover op button.
