# Les 3

Principles of package design. = interessante boek gericht op php.
Clean code (uncle bob).

SOLID principes.
https://scotch.io/bar-talk/s-o-l-i-d-the-first-five-principles-of-object-oriented-design

To read:
https://github.com/phpstan/phpstan/issues/532

## Factory pattern
### Factory class
Een object dat andere objecten gaat maken.

= Een klasse die alle logica bevat om instanties van andere klasses te maken.

2 soorten:
- Simple factory
- Static factory => minder gebruikt.

Voordelen:
- Makkelijk

Nadelen:
- Verstopt complexiteit (depenencies)
- Extra te onderhouden code.

### Factory method
Definieert een interface voor het aanmaken van een object.

