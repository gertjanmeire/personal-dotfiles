# Changes voor de nieuwe merkenarchitectuur (3.x)

- [X] Titels in stijlgids moeten wcag compatible kleur krijgen.
- [X] Maps tints en shades worden 1 map tints met -x en +x values.
- [X] Color function krijgt nieuwe parameter, indien niet 0 roept deze shade() of tint() aan
