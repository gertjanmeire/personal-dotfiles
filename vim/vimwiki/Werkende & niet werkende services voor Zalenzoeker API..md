## Werkende & niet werkende services voor Zalenzoeker API.

Nog reeds NIET werkende service calls na onze testen via Postman.

CHECKLIST:
=============================================================================================================================================================
- GET /rooms/roomId/checkAvailability                             --> zie meeting notes (wordt aangepast door Planon)
- GET /rooms/roomId/unavailabilities                              --> agenda + afwijkende data worden nog niet meegerekend. (wordt aangepast door Planon)
- POST /rooms/roomId/questions                                    --> ERROR: 422 - Invalid body for the request error.
- POST /rooms/roomId/reservations                                 --> ERROR: Geen error, maar reservation of optie wordt niet aangemaakt in Planon.

- POST /reservations/reservationId/questions                      --> ERROR: 422 - Invalid body for the request error.

- GET /questions                                                  --> ERROR: 405 - Method not allowed error.
- GET /questions/statuses                                         --> ERROR: 500 - No operation with name /questions/statuses is registered.
- GET /questions/questionId                                       --> ERROR: 404 not found error

- PATCH /persons/gentUuid                                         --> ERROR: 405 - Method not allowed error.
- POST /persons                                                   --> ERROR: 422 - No person data to create/update error.
