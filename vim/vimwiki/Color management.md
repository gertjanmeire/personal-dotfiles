# Color management

4 basiskleuren
8 secondaire kleuren

Alle kleuren hebben variaties a.d.h.v. opacity changes in vast opgelegde
stappen. Idem voor de donkerdere varianten. (dus in 2 richtingen).
- 6%
- 10%
- 25%
- 25%
- 55%
- 75%

Opacity variabelen voorzien voor alle states die gebruikt zullen worden in
een color() mixin.

We voorzien een color($color, $opacity) mixin.
