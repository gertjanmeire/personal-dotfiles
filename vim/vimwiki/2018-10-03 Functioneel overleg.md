# 2018-10-03 Functioneel overleg

## Actiepunten
- @Jessie: Bestaande landingspagina's (3-tal) voorbeelding uitwerken in design en aan dienst communicatie voorleggen.
- @Jessie: Uitwerken van design voorstel voor subsite logo's indien er geen hero image aanwezig is. Wat met mobiel?
- @Jessie, @Gert-Jan: Bespreken met Jessie ivm de kleurpaletten.
- @Gert-Jan: Bekijken welke marges vaak gebruikt worden op bestaande promopagina's.




### Vragen op pagina in JIRA overlopen
Titels uit wysiwyg halen OK
Titels uitlijning enkel centreren en links. Geen rechtse uitlijning voorzien
Gewicht van titels mogen weggelaten worden.

Link achter een afbeelding --> was oorspronelijk om teasers na te bootsen --> oplossing via alternatieve tijdlijn of teaser weergave.
teaser --> indien link ingevuld --> over hele teaser leggen

tags op teaser mag weggelaten worden = weinig meerwaarde.

Gerelateerde nieuwsberichten paragraaf type: --> nog niet bouwen voor eerste release.
2 opties:
- referentie naar inhoud --> beperkte voor specifieke copy toe te laten
- tool waar redacteur teasers kan kiezen. op basis van bv. tags, keywords, etc ...

Misschien best bestaande landingpage gieten in design als voorbeeld naar dienst communicatie.
3 tal voorbeelden met verschillende componenten laten uiwerken in design ter voorbeeld.

FAQ component ook voorzien op promopagina.




### Custom marges nog nodig?
Bekijken welke marges momenteel gebruikt worden --> welke zijn nog nodig in de nieuwe site?




### Wat doen we met de subsite logo's? In de hero image zoals op scholen? Wat met sites die geen header image hebben?
In geval geen hero image --> design uit laten werken en valideren op design validatie.
Oplossing voor mobiel --> hero image niet laten verwijnen? Te bekijken met Jessie.




### Search design overlopen
Overzicht:
Standaard indien geen zoekterm ingevuld --> geen resultaten.

Op overzicht tonen we zowel vrije openingsuren en op afspraak indien mogelijk op dezelfde dag.
Indien enkel op afspraak tonen we enkel die.
Geen check op overlappende uren nodig.
Telefonisch bereikbaar kanaal wordt niet getoond op overzicht, enkel op detail pagina.
Meer info link hoort net onder de inhoud van de teaser te komen, moet niet onderaan de teaser op dezelfde plaats.


Te indexeren velden:
Sommige content liever niet indexeren in zoekmachines.


Zoekpagina:
Standaard geen resultaten tonen. Enkel bij een gezochte term tonen we de restultaten + filters.
Zoekveld ingegeven term --> geen filter van maken.
Filter op content type --> OR relatie met checkboxes = OK.

Search result teaser --> meta data tags op andere positie? Nu een beetje vreemd.
Indien geen afbeelding tags onder titel, met afbeelding wel boven de titel.


Meertaligheid:
Bij search indien geen resultaten --> link die doorverwijst naar NL zoekpagina zonder de Engelse zoekterm.
In de teasers op het zoekoverzicht tonen we enkel taal neutrale zaken en tonen we geen lees meer link want die gaat naar een NL versie van die pagina.
Ideaal zou zijn als VESTA meertalig wordt.




### Feedback functionaliteit + Gentinfo?
De feedback data zou toch opgenomen moeten worden in het CRM van Gentinfo.
Inhoud van het formulier moet doorgestuurd worden --> nieuwe contactvelden zoals
naam en email, laatste redacteur van de pagina, etc... zouden wss moeten
toegevoegd worden aan het formulier.
--> Graag bij release van de website.
Moet nog goed gekeurd worden door Gentinfo.
