# BA proces samenvatting

## Actiepunten
- Artikel lezen: https://www.frankwatching.com/archive/2017/01/04/zo-ben-je-binnen-een-uur-wegwijs-in-google-analytics/
- Artikel lezen: https://www.simoahava.com/seo/seo-in-a-nutshell-and-some-tips/<Paste>
- Opleiding GA?
- Opleiding SEO?


## Proces flow

---------------------------------------------------------------------------------------------------------------------------------------------------------------
VERKENNING:
---------------------------------------------------------------------------------------------------------------------------------------------------------------
1. Voorgeschiedenis nagaan
  Bestaande documentaties checken is vaak de eerste stap.
  --> Docs / presentaties hierover vind je terug op http://crm365.gentgrp.gent.be/RelatiebeheerEnSolutioning

  --> Voorstellen kan je ook vaak terugvinden op http://shp.gentgrp.gent.be/sites/TeamStrategischCoordinatie/Lists/Ideen%20SG/AllItems.aspx als ze op challenge geweest zijn.

  --> Bestaande toepassingen vind je op http://shp.gent.be/sites/DGApplicaties/SitePages/Welkom.aspx

  --> Confluence = documentatie door webteam. (projecten vanaf 2016 ongeveer)

  --> Langsgaan bij contactpersonen is ook belangrijk! Bv. de applicatiebeheerders.


  ### Vragen
  - Link bij pumtje 1 werkt niet (http://crm365.gentgrp.gent.be/RelatiebeheerEnSolutioning)
  - Hoe werkt challenge precies? Kort woordje uitleg / documentatie?

---------------------------------------------------------------------------------------------------------------------------------------------------------------

2. Bestaande functionaliteit testen
  --> Navigatie testen = visueel gaan voorstellen links in website en waar ze naartoe leiden. Bv. https://digipolisgent.atlassian.net/wiki/spaces/ICLB/pages/135528578/000+Structuur+website
      ==> Inzicht krijgen in data structuur, hoe is info bereikbaar, etc...
      ==> Kritisch bekijken / herzien voor nieuwe site. Vanuit het oogpunt van de doelgroep(en)

  --> Content (types) = Visueel voorstellen van de huidige datatypes en hun attributen.

  --> Functionaliteiten = Lijst van functionaliteiten en wat wie / welke rol kan.
      ==> Functionaliteiten kritisch bekijken.

  --> Processen = gerelateerde processen in kaart brengen.
      ==> Beter begrijpen van de business.

  --> Koppelingen met 3th party systemen = overzicht van met welke 3th party systemen een app connecteert.

  --> Gebruikers en rollen = wie doet wat?


  ### Vragen
  - UC Model V2.0.vsd --> Dit linkt naar een file, maar met welk programma?

---------------------------------------------------------------------------------------------------------------------------------------------------------------

3. Vergelijken met andere websites.

  ### Vragen
  - Ik veronderstel gelijkaardige sites?

---------------------------------------------------------------------------------------------------------------------------------------------------------------

4. Google analytics bekijken.
  --> Bezoekers, probleempunten leren kennen

  --> Meer focus bij verder onderzoek bij bv. interview met stakeholders. Het DOEL is belangrijk! Wat is het belangrijkste probleem, de bestaansreden van de website?

  --> Blijven doorvragen bij opdrachtgever.


---------------------------------------------------------------------------------------------------------------------------------------------------------------

5. Business case documenteren in confluence
	--> Verechtvaardiging budget

  --> Buy-in van stakeholders

  --> Prioriteiten bepalen van requirements (wat dient de business case? wat niet of minder?)

  --> Betrokkenheid creëren bij developers

	--> Wat zijn de voordelen van dit project voor de business?
		--> Indien mogelijk kwantificeerbaar maken.


	### Vragen
	- Voorbeelden van dit soort documentaties zouden handig zijn?


---------------------------------------------------------------------------------------------------------------------------------------------------------------

6. Interview met stakeholders:
	Documenteren voor stakeholders en business case.
		--> Stakeholder Map
		→ In Confluence documenteren wie de key users + stakeholders zijn + contactgegevens vermelden.

		--> Leren begrijpen van betrokken personen / partijen / het project.

		--> BELANGRIJK = luisteren tijdens interviews met gebruikers!

		--> Geen suggestieve vragen!

		--> vragen over specifieke ervaringen beter dan algemeenheden

		--> gebruik volgende key-woorden: waarom, wanneeer, waar, hoe

		--> ontdek de echte pijnpunten of motivatoren door dieper te graven met 'Waarom'

		--> capteer de extacte woorden en expressie die de mensen gebruiken

		--> Observeer mensen!


	### Vragen
	- Voorbeelden van mindmaps / empathy maps binnen team web?


---------------------------------------------------------------------------------------------------------------------------------------------------------------

7. High-level inschatting voor planning & budget:

	### Vragen
	- Is dit niet meer een PM zijn werk dan een BA?



---------------------------------------------------------------------------------------------------------------------------------------------------------------
ONTWERP- EN UITVOERINGSFASE:
---------------------------------------------------------------------------------------------------------------------------------------------------------------
1. Aftoetsen met stakeholders
		--> Meeting vastleggen met key user --> bespreken wat hij wil. (bestaande site = bespreken wat anders moet).
		--> Maak aantekeningen tijdens de meeting, schetsen.
		--> Documenteer na de meeting alles in Confluence.

		--> Wireframes kunnen nu uitgewerkt beginnen worden. (best eerst op papier = paper prototyping).
				Deze papier schetsen bespreken met de UX designer.
				Luister ook eens bij andere analisten of ze dit eventueel al opgelost hebben.
				Indien key user wil kan je de WF ook met hem bespreken. LET OP: dat dit nog met technische mensen besproken MOET worden.

			--> Technisch afchecken bij bv. Dieter (architect), developer, elias (consulent web).


2. Functionele noden beschrijven
		--> Checken of bouwblokken gebruikt kunnen / moeten worden: https://digipolisgent.atlassian.net/wiki/spaces/WEBCAT/pages/193790413/Web+Cataloog+Matrix
		--> Check of bouwblokken die je misschien nodig hebt al opgestart zullen worden: https://digipolisgent.atlassian.net/wiki/spaces/WEBCAT/pages/226689508/Kandidaat+bouwblokken
		--> Bekijk of functionaliteit belangrijk genoeg is om in een bouwblok gestoken te worden.

		--> Afchecken functionaliteit met authentieke bronnen zoals GIS, Master data, Mijn Gent, Vesta, Planon,...
		--> Maak de doelstelling meetbaar: data layer opbouwen, wat dient bv. bijzonder getracked te worden?

		--> Functionaliteiten beschrijven in Confluence:
				Pagina per functionaliteit
					- Link naar epic
					- Link naar Wireframe
					- Link naar design
					- User stories opsommen (+ hun acceptance criteria --> kunnen verder uitgewerkt worden door testers, developers ).
		--> Maak JIRA tickets aan per user story.
		--> Opmaken van het informatiemodel = visuele voorstelling concepten / relaties / condities / ...
				Hiermee kunnen we duidelijke namen afspreken voor concepten!!
		--> Opmaken datamodel samen met lead dev. Bv: https://digipolisgent.atlassian.net/wiki/spaces/KINA/pages/147260633/Datamodel
		--> Design
		--> Overzicht maken van navigatie en pagina templates
				Visueel gaan voorstellen van navigaties tussen pagina's.

3. Planning poker
		--> Met het hele team. Belangrijk hier is dat er issues in JIRA zijn met zoveel mogelijke informatie zodat ze ingeschat kunnen worden.
		Dit geeft veel inzicht door uitwisseling van informatie.

	### Vragen
	- Klopt de zin ivm 2SP per dag is 1SP 2uur werk wel?

4. Sprint planning
		--> Met het hele team + stakeholders. Zonder stakeholders = nadelig.

6. Opvolging
		--> Standby zijn voor devs.

7. Testen & demo
		--> PM of BA geeft aan wat zal getoond worden om de verwachtigen van de stakeholders / gebruikers juist in te stellen.
