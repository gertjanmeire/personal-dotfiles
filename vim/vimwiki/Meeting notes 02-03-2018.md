## Meeting notes 02-03-2018

### Activiteits type issue bij een reservatie.
- Activiteits type die doorgestuurd wordt naar Planon voor een reservatie
te posten zou een nieuw veld moeten krijgen.
Het veld "Soort activiteit" wordt gebruikt en bevat batis gegevens.

Wij zouden 1 van de 21 activiteit types moeten kunnen doorsturen naar een
NIEUW veld "Geplande activiteit".


================================================================================
OPLOSSING:
- Nieuw veld "Geplande activiteit" waarin wij 1 van de 21 types zullen opslaan.
- "Soort" activiteit veld wordt verborgen (is enkel voor Batis)
- "Type" activiteit blijft bestaan en dient voor de facturatie binnen Planon.
================================================================================

### Beschikbaarheden bij het opvragen (zaal detail pagina).
Planon moet de afwijkende tijden voorzien van een gekoppelde kalender
+ geplande activiteiten en dit alles samen doorsturen.

Op de front-end moet 00:00h kunnen ingepland worden op dezelfde dag als de
gebruiker bv. een reservatie van 18u tot 00u dan gaat dit momenteel niet in Planon.


PLANON MOET OOK VOORZIEN DAT ALS WE EEN RESERVATIE DOORSTUREN DAT DEZE GEEN
REKENING HOUDT MET DE GEKOPPELDE KALENDER, ENKEL INGEPLANDE ACTIVITEITEN.

CONCLUSIE:
- checkAvailability call kijkt enkel naar de activiteiten ingepland (NIET de kalender)

- availabilities call kijkt naar zowel kalender en activiteiten + afwijkende uren.
  we voorzien een tekst dat de zaal bepaalde sluitingsuren heeft.
  Planon voorziet types op de unavailabilities die ze terugsturen per range.


 PROBLEEM:
 VEEL API CALLS WERKEN NOG NIET, NIET MEER OF WERKEN NIET ZOALS HET HOORT.

