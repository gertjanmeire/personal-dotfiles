# Retrospective moment
=)
- [ ] Tof team.
- [ ] Tof project.
- [ ] Technische uitdaging is leuk.
- [ ] Blij dat code kwaliteit hier niet vergeten wordt.

-_-
- [ ] Twijfel een beetje aan het feit dat iedereen op dezelfde golflengte zit tov atomic design.
- [ ] Designer vaker beschikbaar op kantoor.

=(
- [ ] Feedback op design komt altijd te laat...
- [ ] Verdelen van resources naar andere projecten / diensten... (Bart die max 1 dag krijgt)
- [ ] Voor design moet meer in wireframing zitten. Afgeklopte wireframes zijn nodig! Design zit hierdoor vast.
- [ ] Volledige tickets accessibiliity info / UX info / design info moeten ok staan voor ze in ontwikkeling komen.
- [ ] Bottleneck design en stijlgids.
- [ ] Proactiviteit van alle partijen? https://digipolisgent.atlassian.net/wiki/spaces/SG8/pages/123995976/Header+footer wat hier?
- [ ] Beetje teveel werk voor mezelf binnenkort? Gaan we kunnen meebenen eens we integratie met Drupal moeten doen ook met 1 developer?
- [ ] Hoeveel tijd kan de 2de desginer uitrekken voor het project? Komt nogal over dat het niet veel tijd is...
- [ ] Communicatie tussen alle partijen.



## Validatie criteria:
- UX
- Accessibility
