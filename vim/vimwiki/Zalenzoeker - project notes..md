# Zalenzoeker - project notes.

ZALENZOEKER DEVELOPMENT LIGT STIL TOT DE PLANON SERVICES TERUG 100% IN ORDE
GEBRACHT ZIJN DOOR PLANON.

## Services
- [Werkende & niet werkende services voor Zalenzoeker API.](Werkende & niet werkende services voor Zalenzoeker API.)


## Tickets
- [ ] [checkAvailability call bij schrijven op zalenzoeker_package.](checkAvailability call bij schrijven op zalenzoeker_package.)


## Todo's
- [ ] [Verwijzing naar QA01 voor nieuwe webservice opzetten.](Verwijzing naar QA01 voor nieuwe webservice opzetten.)
- [X] Werkende krijgen van Gent authenticatie block.


## Meeting notes
- [Meeting notes 02-03-2018](Meeting notes 02-03-2018)
    Meeting i.v.m. onduidelijkheden rond de opgeleverde service op dev01


