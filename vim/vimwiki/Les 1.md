## Intro

Inheritance:
- argumenten mogen niet van type veranderd worden
- nieuwe argumenten mogen, maar moeten null zijn.

-------------------------------------------------------------------------------

Abstract classes.
- Minimale visiblity: protected.
- Definieert method signature, GEEN implementatie.

-------------------------------------------------------------------------------

Inteface
- Definieert enkel public methods
-  Voordeel --> contracten als we werken met typehints.
- Laten multiple extends toe van elkaar (ENKEL interfaces!)

-------------------------------------------------------------------------------

Traits:
- Compile time copy paste van code in een klasse.
- Mogelijk meerdere Traits in een klasse te gebruiken.
- [Horizontal composition of behavior](Horizontal composition of behavior)

-------------------------------------------------------------------------------

Scope:
- $this --> object scope
- self = referentie defining class
- parent = referentie parent class
- static = referentie runtime class (Late static binding)

-------------------------------------------------------------------------------

Late static binding:
Statis is late static binding --> dat wil zeggen als een overervende klasse static::class aanroept we de overervende klasse krijgen.
Bv.

        <?php

        namespace Sites\oop_opleiding;

        /**
        * Class late_static_binding
        * @author yourname
        */
        class Foo
        {
          public static function dump() {
            var_dump(self::class);
            var_dump(static::class);
          }
        }

        /**
        * Class lSites\oop_opleiding
        * @author yourname
        */
        class Bar
        {
        }

        Test::dump(); // --> geeft Foo en arn Bar


-------------------------------------------------------------------------------

Final keyword markeert een klasse die niet meer overschreven kan worden.
Geen extends meer mogelijk van deze klasse.
Werkt NIET op properties!

-------------------------------------------------------------------------------

Overloading werkt niet native in php.
--> __call() magic method
= last resort method om multiple gelijkaardige methods op te gaan vangen.
( = workarround )

-------------------------------------------------------------------------------
