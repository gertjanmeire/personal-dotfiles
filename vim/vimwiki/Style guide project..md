# Style guide project.

## TODO's
Opmerkingen design / wireframing
- Spot images moeten finaal beslist worden (Design)
- Tags zijn al deels gemaakt maar zitten nog op het TODO stadia van de design sprint (Analyze, UX, Design)
- Responsive gedrag breadcrumbs?? (Analyze, UX, Design)
- Header gedrag + mobile design? (Analyze, UX, Design)
- Info pagina
  - H2 - H5 moeten bepaald worden voor gebruiken binnen wysiwyg teksten. (UX, Design)
  - CTA design desktop + mobile?
  - Validatie info page ook nog interessant box.
  - Gedrag header image info pagina op mobiel?
  - Caption niet wcag compliant
  - Finale definitie + desktop en mobiel design voor het feedback form
  - Timeline info pagina moet nog afgewerkt worden + mobiel design?

### Vragen voor Jeroen / Bart:

### Vragen voor Johan:

### Vragen voor Jessie:
- [ ] Marges en line heights voor headers moeten consistent gemaakt worden!
- [ ] Focus states moeten gedesigned worden op componenten waar nodig.
- [ ] Footer telefoon link op mobiel is button styling. Maken we hiervoor een
      mobile only klasse ofzo aan?
- [ ] button kleuren zijn nog niet ok
- [ ] input atomen success en error states hun kleuren zijn nog niet allemaal ok
- [ ] input atomen radio en checkboxes moeten nog states krijgen in design
- [ ] buttons en links accordion mag er niet uitzien als link = inconsequent
- [ ] headings moeten vastgelegd worden, ook in tekstparagrafen
- [ ] uniforme bottom marges vastleggen voor tussen blokken en paragrafen

-------------------------------------------------------------------------------
## [Changes voor de nieuwe merkenarchitectuur (3.x)](Changes voor de nieuwe merkenarchitectuur (3.x))
-------------------------------------------------------------------------------
## [Color management](Color management)
-------------------------------------------------------------------------------
## [Meetings](Meetings)
-------------------------------------------------------------------------------
## [Restrospective](Restrospective)
-------------------------------------------------------------------------------
