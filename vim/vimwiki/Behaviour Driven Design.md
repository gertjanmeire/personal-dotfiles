# Opleiding Behaviour Driven Design

## Intro
Lesgever: Bart Wullems.
bart.wullems@ordina.be
https://github.com/wullemsb

Gekende problemen bij testen:
- Tijdsintensief
- Druk
- Onrealistisch testen
- Meerwaarde uitgelegd krijgen aan de business
- Onduidelijke requirements
- ...


## TDD
BDD begint met Test Driven Development (TDD).
TDD -->
- Red = analysis
- Green = Creative part
- Refactor = engineering

Problematiek TDD:
--> Veel discipline nodig om op deze manier te werken!
--> High percentage aan code coverage.
--> Streamlined codebase.
--> Cleanere interfaces.
--> Some sort of documentation for the code.
--> Er is een perceptie dat development trager gaat.


## BDD
BDD is geen:
- testing framework
- methode om testen te schrijven

BDD is wel:
- Een template voor User Stories
- In staat om testbare User Stories te maken omdat ze gedefinieerd zijn in een
  formele manier.

Requirements fase is belangrijk! Het zorgt ervoor dat we testen makkelijker
kunnen gaan schrijven.
--> Beschrijf wat een systeem ZOU MOETEN DOEN en niet hoe het gedaan wordt.

BDD merges 2 different approaches:
- User Stories --> alles exact hier schrijven is niet realistisch en niet nodig!
  **Een conversatie met de business aangaan is heel belangrijk!**
- Domain Driven Design --> bepaalde terminlogie gaan definieren zodat het voor
                           iedereen duidelijk is.

BDD is:
- Using examples
- a shared understanding: gelijke terminologie
- surface uncertainty: wat is onduidelijk?
- software that matters: waar is het belangrijk om BDD toe te passen?
- automatische acceptatie criteria
- Collaboratie tussen elkaar

Vaak samenzitten met de business is essentieel! De continue dialoog!




