# Notes wiki

# Important TODO's
-------------------------------------------------------------------------------


## Personal notes
-------------------------------------------------------------------------------
- [TODOS](TODOS)
- [3D printing](3D printing)
- [Prop](Prop)



## General
-------------------------------------------------------------------------------
- [Gepland verlof](Verlof)
- [BA meeting meevolgen](BA meeting meevolgen)
- [BA proces samenvatting](BA proces)



## Conferenties
-------------------------------------------------------------------------------
- [Drupal dev days 2018.](Drupal dev days 2018.)



## Opleidingen
[](-------------------------------------------------------------------------------)
- [OOP PHP opleiding.](OOP PHP opleiding.)
- [Behaviour Driven Design](Behaviour Driven Design)
- [WCAG 2.1 opleiding](WCAG 2.1 opleiding)



## Projects
### Style guide.
-------------------------------------------------------------------------------
- [Style guide project.](Style guide project.)
- [Style guide overleg](Style guide overleg)
- [Externe component style guide](Externe component style guide)
- [Meeting ivm Angular / Vue](Meeting ivm Angular / Vue)



### Stad Gent D8.
-------------------------------------------------------------------------------
- [Stad Gent Drupal 8.](Stad Gent Drupal 8.)
- [Analyse promopaginas.](Analyse promopaginas.)
- [Meeting met IA](Meeting met IA)


### Functionele meetings Stad Gent D8.
-------------------------------------------------------------------------------
[](-) [2018-10-03 Functioneel overleg](2018-10-03 Functioneel overleg)


### Zalenzoeker
-------------------------------------------------------------------------------
- [Zalenzoeker - project notes.](Zalenzoeker - project notes.)
