# Meeting IA 02/10/2018 - Stad Gent

6-tal toptaken + 10-tal medium taken.

Uitwerken van 1 test scenario per top taak --> test personen deze taak laten uitvoeren.

## Toptaak 1
Contacten, openingsuren en adressen = toptaak 1.
--> bijna 70% komt niet op de doelpagina uit!



## Belangrijke opmerkingen
- Veel dubbele of overbodige content
- Navigatiestructuur kan beter



## Navigatie inzichten
Door navigatie te herorganiseren zien we een duidelijke improvement in het
vinden van content door eindgebruikers.

### Enkele bevindingen:

-Meldingen vragen en suggesties trekken atm veel kliks... dit moet duidelijker
gemaakt worden.
- Vergelijkbare pagina's en labels vermijden
  --> Labels moeten concreter gemaakt worden zodat gebruikers hun weg makkelijker vinden.

Getest via:

- Card sorting
- Tree test



## Zoek functionaliteit

SPI --> search performance indicator test
Belangrijk zowel algoritmes als content ingave!



## Aandachtspunten

- Veel dubbele content op versch. plaatsen
- Zeer veel content: Bv in Cultuur en vrije tijd
- Zeer lange pagina's Bv. ID aanvraag
- Zoeken binnen de website presteert matig door veel vergelijkbare content.
- Structuur van bv. de promopagina's ==> LEZ pagina.
- Header en breadcrumb op desktop en mobile niet altijd optimaal
- Hero images moeten compacter op mobile
- Uit in Gent is moeilijk toegankelijk vanaf stad.gent
- Samenwerking lastig te controleren voor dienstoverkoepelende pagina's


