# Analyse promopagina's

Thema bepalen op basis van secundaire stijlgids kleuren (voor volledige pagina, NIET per paragraaf type.).

Altijd titel in html --> maar vinkje om titel visueel aan / uit te zetten op promopagina'[s](s.)

Titels niet in wysiwyg instelbaar op promopagina's --> op andere pagina's wel nog.


MUST HAVE:
V Kaart
V Tekst
V titel
V quote
V cta
V nieuwsbrief
V embedded item
V afbeelding / gallery
V video
V teaser
V hero
V tijdlijn
V tijdlijn alternatief


OPTIONEEL:
contact
faq
quote met foto
hero afbeelding
hero afbeelding subsite
gerelateerde nieuwsberichten / evenementen
