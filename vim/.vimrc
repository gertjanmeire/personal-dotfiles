" -----------------------------------------------------------------------------------------------------------------------
" -----------------------------------------------------------------------------------------------------------------------
" @NOTE:
" We should run MacVim from the terminal instead of the version of vim that
" comes with MacOSX, it's a lot faster!!!
" This is done by moving the 'mvim' executable to the /usr/local/bin folder
" and symlink it to the 'vim' command
" http://effectif.com/vim/running-vim-with-ruby-support-in-terminal
" -----------------------------------------------------------------------------------------------------------------------
" -----------------------------------------------------------------------------------------------------------------------
"
"
"  -----------------------------------------------------------------------------------------------------------------
" Visual and gereral stuff
" ------------------------------------------------------------------------------------------------------------------
let mapleader=","                                 " change the mapleader from \ to ,

syntax on                                         " Put on syntax highlighting
autocmd BufEnter * :syntax sync fromstart         " Fix syntax highlighting in larger files

set background=dark
colorscheme hybrid
let g:enable_bold_font = 1
let g:enable_italic_font = 1

filetype plugin on
set omnifunc=syntaxcomplete#Complete

set number                                        " Set line numbers
set ignorecase                                    " Ignore cases when searching
set smartcase                                     " Only search case sensitive if there is a capital letter in the search
set ruler                                         " Always show info along bottom
set colorcolumn=80                                " Add a colored line to show the max text width.
set tabstop=2                                     " Set tab spacing
set softtabstop=2
set shiftwidth=2
set expandtab                                     " In Insert mode: Use the appropriate number of spaces to insert a <Tab>.
set backspace=2                                   " Backspace deletes like most programs in insert mode"
set shiftround
set autoindent
set smartindent
set cindent
set hidden                                        " Hide buffers instead of closing them
set history=50                                    " Increase search history
set undolevels=1000                               " Increase undo levels
set title                                         " Set terminal title to file title
set nobackup                                      " Set not backup or swp files
set noswapfile                                    " Do not generate a swap file
set pastetoggle=<F2>                              " Temp switch to paste mode when pasting from outside terminal to avoid weird formatting and indenting
set hlsearch                                      " Highlight as you search
set incsearch                                     " ...dynamically as they are typed."
set wildmenu                                      " visual autocomplete for command menu"
set showmatch                                     " Show matching parenthesis
set autoread                                      " Watch for file changes
set ttyfast                                       " We have a fast terminal
set incsearch                                     " Seach settings
set showmatch                                     " Show matching search
set hlsearch                                      " Highlight search result
set ttyfast                                       " Indicate we have a fast terminal
set lazyredraw                                    " Do not redraw the screen when using macro's and commands
set foldenable                                    " enable folding
set foldmethod=indent                             " Setup fold method
set foldcolumn=2                                  " Show what will be folded in the sidebar
" set cursorline                                    " Highlight current line  --> HIGH TOLL ON REDRAW RATE = SLOWER PERFORMANC
set laststatus=2                                  " Always show a status line (2) - See the :help for more options
set showcmd                                       " Display incomplete commands
set list listchars=tab:»·,trail:·,nbsp:·          " Display extra whitespace
set keywordprg=":help"                            " With this when pressing Ctrl + K vim searches for the documentation of the word under the cursor in the vim docs instead of a man page entry
set encoding=utf8


"  -----------------------------------------------------------------------------------------------------------------
" Enforce consistent line endings: if 'ff' is set to "unix" and there are any
" stray '\r' characters at ends of lines, then automatically remove them. See
" $VIMRUNTIME/indent/php.vim .
"  -----------------------------------------------------------------------------------------------------------------
let PHP_removeCRwhenUnix = 1


"  -----------------------------------------------------------------------------------------------------------------
" Remaps (standard)
"  -----------------------------------------------------------------------------------------------------------------
"
" Normal mode key mappings
"
nnoremap <C-e> 3<C-e>                             " Move faster down a file
nnoremap <C-y> 3<C-y>                             " Move faster down a file
nnoremap <leader><space> :noh<cr>                 " Clear search result highlighting
nnoremap <leader>ev :vsp $MYVIMRC<CR>             " type,evto edit the Vimrc
nnoremap <leader><leader> <c-^>                   " Switch between the last two files
nnoremap <space> za                               " Space toggle fold
nnoremap <C-o> zA                                 " Toggle folds and their children

"
" Virtual mode key mappings
"
nnoremap <leader>c :%w !pbcopy<CR>                " Easy copy to clipboard
nnoremap <leader>v :r !pbpaste<CR>                " Easy paste from clipboard in virtual mode

"
" Insert mode key mappings
"
inoremap jj <ESC>                                 " Quickly escape in insert mode to normal mode


"  -----------------------------------------------------------------------------------------------------------------
" Vundle stuff to manage plugins
"  -----------------------------------------------------------------------------------------------------------------
set nocompatible                                    " be iMproved, required
filetype off                                        " required

"
" set the runtime path to include Vundle and initialize
"
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
"
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')
"

"
" let Vundle manage Vundle, required
"
Plugin 'gmarik/Vundle.vim'


"  -----------------------------------------------------------------------------------------------------------------
" PLUGINS GO HERE
"  -----------------------------------------------------------------------------------------------------------------
Plugin 'bling/vim-airline'                                " Airline is a status line for VIM
Plugin 'tpope/vim-fugitive'                               " vim-fugitive: Git wrapper for VIM
Plugin 'airblade/vim-gitgutter'                           " Git gutter to show in file diff
Plugin 'Lokaltog/vim-easymotion'                          " Easily move in files
Plugin 'terryma/vim-multiple-cursors'                     " Support for multiple cursor like ST2
Plugin 'docunext/closetag.vim'                            " CLose HTML tags with a single button
Plugin 'matchit.zip'                                      " Extend % command to work with HTML tags (jump between opening and closing tag)
Plugin 'Raimondi/delimitMate'                             " Auto close brackets and stuff
Plugin 'sjl/gundo.vim'                                    " Visualize your undo tree
Plugin 'YankRing.vim'                                     " Improve copy paste functionality
Plugin 'scrooloose/nerdtree'                              " Replace standard vim explorer
Plugin 'tpope/vim-repeat'                                 " Repeat actions from plugins with the '.' dot command
Plugin 'spf13/PIV'                                        " PHP integration environment
Plugin 'benmills/vimux'                                   " Run Tmux commands inside VIM
Plugin 'joonty/vdebug'                                    " Add xDebug // after this run on command line: vim +BundleInstall +qall
Plugin 'majutsushi/tagbar'                                " Support for Tags from Ctags
Plugin 'ntpeters/vim-better-whitespace'                   " Show and remove unwanted whitespace
Plugin 'loremipsum'                                       " Lorem ipsum generator
Plugin 'sickill/vim-pasta'                                " Override p and P for pasting with correct indentation.
Plugin 'scrooloose/syntastic'
Plugin 'stephpy/vim-php-cs-fixer'
Plugin 'arnaud-lb/vim-php-namespace'
Plugin 'foosoft/vim-argwrap'
Plugin 'Atomic-Save'
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'
Plugin 'ludovicchabant/vim-gutentags'
Plugin 'ryanoasis/vim-devicons'
Plugin 'vimwiki/vimwiki'
Plugin 'itchyny/calendar.vim'
Plugin 'shougo/vimproc.vim'





" ------------------------------------------------------------
" CODE SUPPORT PLUGINS
" ------------------------------------------------------------
Plugin 'cakebaker/scss-syntax.vim'                        " Syntax highlighting for sass
Plugin 'hail2u/vim-css3-syntax'                           " Syntax highlighting for css3n ''
Bundle 'git://github.com/miripiruni/csscomb-for-vim.git'
Plugin 'othree/html5.vim'                                 " Syntax highlighting for new HTML5 elements
Plugin 'pangloss/vim-javascript'                          " Syntax highlighting for javascript
Plugin 'elzr/vim-json'                                    " Better JSON support
Plugin 'nono/jquery.vim'                                  " JQuery support
Plugin 'beyondwords/vim-twig'                             " Enable Twig syntax highlighting
Plugin 'craigemery/vim-autotag'
Plugin 'leafgarland/typescript-vim'
Plugin 'galenframework/galen.vim'
Plugin 'vim-airline/vim-airline-themes'



"
" All of your Plugins must be added before the following line
"
call vundle#end()            " required
filetype plugin indent on    " required
"
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
"

augroup vimrcEx
  autocmd!

  " When editing a file, always jump to the last known cursor position.
  " Don't do it for commit messages, when the position is invalid, or when
  " inside an event handler (happens when dropping a file on gvim).
  autocmd BufReadPost *
        \ if &ft != 'gitcommit' && line("'\"") > 0 && line("'\"") <= line("$") |
        \   exe "normal g`\"" |
        \ endif

  " Set syntax highlighting for specific file types
  autocmd BufRead,BufNewFile *.md set filetype=markdown

  " Automatically wrap at 72 characters and spell check git commit messages
  autocmd FileType gitcommit setlocal textwidth=72
  autocmd FileType gitcommit setlocal spell

  " Allow stylesheets to autocomplete hyphenated words
  autocmd FileType css,scss,sass setlocal iskeyword+=-
  autocmd FileType php set omnifunc=phpcomplete#CompletePHP
augroup END

"
" Autocmd's
"
if has("autocmd")
  "Drupal *.module and *.install files.
  augroup filetypedetect
    au! BufRead,BufNewFile *.module setfiletype php
    au! BufRead,BufNewFile *.theme setfiletype php
    au! BufRead,BufNewFile *.install setfiletype php
    au! BufRead,BufNewFile *.test setfiletype php
    au! BufRead,BufNewFile *.inc setfiletype php
    au! BufRead,BufNewFile *.profile setfiletype php
    au! BufRead,BufNewFile *.view setfiletype php
  augroup END
endif

"
" Treat <li> and <p> tags like the block tags they are
"
let g:html_indent_tags = 'li\|p'

"
" Open new split panes to right and bottom, which feels more natural
"
set splitbelow
set splitright

"
" Quicker window movement
"
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l


" ------------------------------------------------------------
" 1. VIM Airline settings
" ------------------------------------------------------------
let g:airline#extensions#tabline#enabled = 1               " Set the airline enabled on startup
let g:airline#extensions#whitespace#enabled = 0           " don't count trailing whitespace since it lags in huge files
let g:airline_theme = "hybrid"


" ------------------------------------------------------------
" UltiSnippets settings
" ------------------------------------------------------------
"
" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
"
let g:UltiSnipsExpandTrigger="<c-e>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"


"
" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"
let g:ycm_register_as_syntastic_checker = 0
let g:ycm_collect_identifiers_from_tags_files = 0

let g:UltiSnipsSnippetDirectories=[$HOME.'/.vim/UltiSnips']


" ------------------------------------------------------------
" DelimitMate settings
" ------------------------------------------------------------
let g:delimitMate_expand_cr = 1
let g:delimitMate_expand_space = 1


" ------------------------------------------------------------
" Gundo settings
" ------------------------------------------------------------
nnoremap <leader>u :GundoToggle <CR>                      " Map <leader>u to toggle the undo window
let g:gundo_close_on_revert = 1                           " Close the window on selecting a revert


" ------------------------------------------------------------
" YankRing Settings
" ------------------------------------------------------------
nnoremap <leader>y :YRShow <CR>                               " Shortcut to show the YankRing
let g:yankring_clipboard_monitor = 1



" ------------------------------------------------------------
" NERDTree settings
" ------------------------------------------------------------
nnoremap <leader>o :NERDTreeToggle<CR>
let NERDTreeShowHidden=1                                      " Show hidden files by default
let g:NERDTreeChDirMode=2                                        " Change NERDTree directory to vim's directory


" ------------------------------------------------------------
" Multiple Cursor Settings
" ------------------------------------------------------------
let g:multi_cursor_use_default_mapping=0                      " Disable default key mappings of multiple-cursors
let g:multi_cursor_next_key='<C-m>'
let g:multi_cursor_quit_key='<Esc>'


" ------------------------------------------------------------
" Argwrap
" ------------------------------------------------------------
nnoremap <silent> <leader>z :ArgWrap<CR>


" ------------------------------------------------------------
" Fugitive settings
" ------------------------------------------------------------
nnoremap <leader>gs :Gstatus <CR>
nnoremap <leader>gc :Gcommit <CR>
nnoremap <leader>gd :Gdiff <CR>
nnoremap <leader>gl :Glog <CR>
nnoremap <leader>gp :Gpush <CR>
nnoremap <leader>gpu :Gpull <CR>


" ------------------------------------------------------------
" FZF settings
" ------------------------------------------------------------
nnoremap <leader>p :Files<CR>
nnoremap <leader>b :Buffers<CR>                                                      " Map the Ctrl+b key combination to open FZF in buffer mode
nnoremap <leader>t :Tags<CR>                                                         " Map the Ctrl+t key combination to open FZF in tag mode (ctags)
nnoremap <leader>c :Commits<CR>                                                         " Map the Ctrl+t key combination to open FZF in tag mode (ctags)
nnoremap <leader>l :Lines<CR>                                                         " Map the Ctrl+t key combination to open FZF in tag mode (ctags)


" ------------------------------------------------------------
" TagBar settings
" ------------------------------------------------------------
nnoremap <leader>n  :TagbarToggle <CR>


" ------------------------------------------------------------
" Gist-vim settings
" ------------------------------------------------------------
"nnoremap <leader>G :Gist -l <CR>


" ------------------------------------------------------------
" VIM Better whitespace
" ------------------------------------------------------------
let strip_whitespace_on_save = 1                                                    " Automatically call this so on every save we strip whtiespace


" ------------------------------------------------------------
" Vimux settings (Tmux commands from inside VIM)
" ------------------------------------------------------------
"
" Prompt for a command to run
"
map <Leader>vp :VimuxPromptCommand<CR>

" ------------------------------------------------------------
" VDebug settings
" ------------------------------------------------------------
let g:vdebug_options = {'ide_key': 'PHPSTORM'}
let g:vdebug_options = {'break_on_open': 1}
let g:vdebug_options = {'server': '127.0.0.1'}
let g:vdebug_options = {'port': '9000'}
"
" Vdebug
"
let g:vdebug_keymap = {
      \    "run" : "<Leader>r",
      \    "run_to_cursor" : "<Leader>xc",
      \    "step_over" : "<Leader>xo",
      \    "step_into" : "<Leader>xi",
      \    "step_out" : "<Leader>xe",
      \    "close" : "q",
      \    "detach" : "x",
      \    "set_breakpoint" : "<Leader>xb",
      \    "eval_visual" : "<Leader>e"
      \}

let g:vdebug_options = {'ide_key': 'PHPSTORM'}
let g:vdebug_options = {'break_on_open': 1}
let g:vdebug_options = {'server': '127.0.0.1'}
let g:vdebug_options = {'port': '9000'}
"
" Vdebug
"
let g:vdebug_options= {
  \    "port" : 9000,
  \    "server" : '',
  \    "timeout" : 20,
  \    "on_close" : 'detach',
  \    "break_on_open" : 0,
  \    "ide_key" : '',
  \    "path_maps" : {},
  \    "debug_window_level" : 0,
  \    "debug_file_level" : 0,
  \    "debug_file" : "",
  \    "watch_window_style" : 'compact',
  \    "marker_default" : '⬦',
  \    "marker_closed_tree" : '▸',
  \    "marker_open_tree" : '▾'
\ }


" ------------------------------------------------------------
" Dbext setup
" ------------------------------------------------------------
" MySQL
let g:dbext_default_profile_mysql_local = 'type=MYSQL:user=root:passwd=root:dbname=testdb'


" ------------------------------------------------------------
" Super handy little stuff!
" ------------------------------------------------------------
"
" Allow saving of files as sudo when I forgot to start vim using sudo.
"
cmap w!! w !sudo tee > /dev/null %

"
" Syntastic
"
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
set statusline+=%F


let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:syntastic_php_phpcs_args = " --standard=Drupal --extensions=php,module,inc,install,test,profile,theme"
let g:syntastic_phpcs_conf='--standard=DrupalCodingStandard'

if has('statusline')
  set laststatus=2
  " Broken down into easily includeable segments
  set statusline=%<%f\ " Filename
  set statusline+=%w%h%m%r " Options
  set statusline+=%{fugitive#statusline()} " Git Hotness
  set statusline+=\ [%{&ff}/%Y] " filetype
  set statusline+=\ [%{getcwd()}] " current dir
  set statusline+=%#warningmsg#
  set statusline+=%{SyntasticStatuslineFlag()}
  set statusline+=%*
  let g:syntastic_enable_signs=1
  set statusline+=%=%-14.(%l,%c%V%)\ %p%% " Right aligned file nav info
endif

" ------------------------------------------------------------
" CSS Comb configuration.
" ------------------------------------------------------------
" Map bc to run CSScomb. bc stands for beautify css
autocmd FileType css noremap <buffer> <leader>z :CSScomb<CR>
" Automatically comb your CSS on save
autocmd BufWritePre,FileWritePre *.css,*.less,*.scss,*.sass silent! :CSScomb


" ------------------------------------------------------------
" PHP namespace.
" ------------------------------------------------------------
function! IPhpInsertUse()
    call PhpInsertUse()
    call feedkeys('a',  'n')
endfunction
autocmd FileType php inoremap <Leader>u <Esc>:call IPhpInsertUse()<CR>
autocmd FileType php noremap <Leader>u :call PhpInsertUse()<CR>

function! IPhpExpandClass()
    call PhpExpandClass()
    call feedkeys('a', 'n')
endfunction
autocmd FileType php inoremap <Leader>e <Esc>:call IPhpExpandClass()<CR>
autocmd FileType php noremap <Leader>e :call PhpExpandClass()<CR>

autocmd FileType php inoremap <Leader>s <Esc>:call PhpSortUse()<CR>
autocmd FileType php noremap <Leader>s :call PhpSortUse()<CR>

let g:php_namespace_sort_after_insert = 1

" ------------------------------------------------------------
" Vimwiki.
" ------------------------------------------------------------
" vimwiki stuff "
let g:vimwiki_list = [{'path': '~/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}]
:map <Leader>tt <Plug>VimwikiToggleListItem

" ------------------------------------------------------------
" Calendar.
" ------------------------------------------------------------
let g:calendar_google_calendar = 1
let g:calendar_google_task = 1
